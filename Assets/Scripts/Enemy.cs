﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed;
    public Char player;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.x < -0.4f && this.transform.position.y == 0)
        {
            this.transform.Translate(speed * Time.deltaTime, 0, 0);
        }
        else if (this.transform.position.x > 0.4f && this.transform.position.y == 0)
        {
            this.transform.Translate(-(speed * Time.deltaTime), 0, 0);
        }
        else if (this.transform.position.x == 0 && this.transform.position.y > 0.4f)
        {
            this.transform.Translate(0, -(speed * Time.deltaTime), 0);
        }
        else if (this.transform.position.x == 0 && this.transform.position.y < -0.4f)
        {
            this.transform.Translate(0, speed * Time.deltaTime, 0);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Slash")
        {
            player.score++;
            Debug.Log(player.score);
            Destroy(this.gameObject);
        }
        if (collision.gameObject.tag == "Player")
        {
            player.score -= player.score/10 + 5;
            Debug.Log(player.score);
            Destroy(this.gameObject);
        }
    }
}
