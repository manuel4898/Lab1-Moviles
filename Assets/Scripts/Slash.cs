﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slash : MonoBehaviour
{
    private void Start()
    {
        StartCoroutine(Kaboom());
    }

    IEnumerator Kaboom()
    {
        yield return new WaitForSeconds(0.05f);
        Destroy(this.gameObject);
    }
    
}
