﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class EventButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Text texto;

    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        texto.text = name;
    }
    void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
    {
        texto.text = "Idle";
    }

}
